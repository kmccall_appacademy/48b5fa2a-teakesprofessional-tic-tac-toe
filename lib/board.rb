class Board
  attr_accessor :grid

  def initialize(grid =
                        [ [nil, nil, nil], #tic tac toe grid
                          [nil, nil, nil],
                          [nil, nil, nil]])

     @grid = grid
  end

  def place_mark(pos, mark) #places your mark on the grid
      # if empty?(pos)
      grid[pos[0]][pos[1]] = mark
    # else
    #   print "the space is not empty"
    # end
     self
  end

  def empty?(pos) # checks to see if the position passed is empty
    grid[pos[0]][pos[1]] == nil
  end

  def over?
    new_arr = []
    if self.winner
      return true
    end
    self.grid.each do |row|
      if row.all? {|x| x && x != nil}
         new_arr << 1
      end
      if new_arr.length == 3
        return true
      end
    end
      false
  end

  def winner
    new_arr = []
    self.grid.each do |row|
      if row.all? {|x| x && x != nil}
         new_arr << 1
      end
      if new_arr.length == 3
        return puts "Tied Game"
      end
    end
    self.grid.each do |row| #horizontal row win check
      if row.all? {|x| x && x != nil} && !(row.include?(:X) &&  row.include?(:O))
        return row[0]
      else
      end
    end
    if self.grid[0][0] == self.grid[1][1] && self.grid[1][1] == self.grid[2][2] && self.grid[0][0] != nil
      return self.grid[0][0]  # diagonal
    elsif self.grid[0][2] == self.grid[1][1] && self.grid[1][1] == self.grid[2][0] && self.grid[0][2] != nil
      return self.grid[0][2]   #diagonal
    elsif self.grid[0][0] == self.grid[1][0] && self.grid[1][0] == self.grid[2][0] && self.grid[2][0] != nil
      return self.grid[0][0]
    elsif self.grid[0][1] == self.grid[1][1] && self.grid[1][1] == self.grid[2][1] && self.grid[1][1] != nil
      return self.grid[0][1]  #second column
    elsif self.grid[0][2] == self.grid[1][2] && self.grid[1][2] == self.grid[2][2] && self.grid[1][2] != nil
      return self.grid[2][2]  #third column
    else

    end
  end
end
