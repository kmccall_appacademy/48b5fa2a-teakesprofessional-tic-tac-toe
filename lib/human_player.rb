class HumanPlayer
  attr_accessor :board, :mark
  def initialize(name = "fred")
    @name = name
    @mark = mark
  end

  def name
    @name
  end

  def display(board)
    print board.grid
  end

  def get_move
    print "Please Enter where you want to move in the Form [x, y]"
     position = gets.chomp
     position.split(", ").map { |e| e.to_i }
  end
end
