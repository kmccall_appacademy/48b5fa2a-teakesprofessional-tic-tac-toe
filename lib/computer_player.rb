
class ComputerPlayer
  attr_accessor :mark
  attr_reader :board
  def initialize(name = "MasterAI")
    @name = name
  end


  def get_move
    length = possible_moves.length
    possible_moves.each do |move|
      if win?(move)
        return move
      end
    end
    possible_moves.sample
  end

  def possible_moves
    possible = []
    # table = self.display(board)
    board.grid.each_with_index do |row, idx|
      row.each_index do |idx_2|
        if row[idx_2] == nil
          possible << [idx, idx_2]
        end
      end
    end
    possible
  end

  def win?(move)
    board.place_mark(move, mark)
    if board.winner
      board.place_mark(move, nil)
      true
    else
      board.place_mark(move, nil)
      false
    end
  end

  def display(board)
    @board = board
  end


end
