require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :current_player,  :board
  attr_writer :player_one, :player_two

  def initialize(player_one= HumanPlayer.new, player_two= ComputerPlayer.new)
    @player_one = player_one
    player_one.mark = :X
    @player_two = player_two
    player_two.mark = :O
    @board = Board.new
    @current_player = player_one
    # play_turn
  end

  def play
    @current_player.display(@board)
    until board.over?
      play_turn
    end
    puts "you win!"
  end

  def play_turn
      @current_player.display(board)
      move = current_player.get_move
      mark = current_player.mark
      @board.place_mark(move, mark)
      switch_players!
  end

  def board
    @board
  end

  def switch_players!
    # @player_two.display(board)
    if current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end
end


if __FILE__ == $PROGRAM_NAME

    #puts "foo"

    this_game=Game.new()
    this_game.play

end
